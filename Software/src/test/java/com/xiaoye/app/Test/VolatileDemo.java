package com.xiaoye.app.Test;

public class VolatileDemo implements Runnable {
	private int number = 0;

	public int getNumber() {
		return this.number;
	}

	public void addNumber() {
		try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		synchronized (this) {
			number++;
			System.out.println(number);
		}
	}

	public static void main(String[] args) {
		VolatileDemo volatileDemo = new VolatileDemo();
		for (int i = 0; i < 500; i++) {
			new Thread(volatileDemo).start();
			System.out.println(volatileDemo.number);
		}
	}

	public void run() {
		addNumber();

	}

}
