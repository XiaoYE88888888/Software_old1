package com.xiaoye.app.service.impl;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xiaoye.app.common.MsgReturn;
import com.xiaoye.app.mapper.CourseMapping;
import com.xiaoye.app.service.CourseService;
import com.xiaoye.app.util.PropertyCodeMsgUtil;

@Service
public class CourseServiceImpl implements CourseService {
	@Autowired
	private CourseMapping courseMapping;
	private Logger logger = LogManager.getLogger(CourseServiceImpl.class);

	@Override
	public MsgReturn getCourseList() throws SQLException {
		List<Map<String, Object>> result = null;
		result = courseMapping.getCourseList();
		Map<String, Object> root = new HashMap<String, Object>();
		root.put("id", "RT0");
		root.put("name", "课程管理");
		root.put("open ", "true");
		root.put("noRemoveBtn ", "true");
		root.put("noEditBtn ", "true");
		root.put("iconOpen", "zTree_v3/css/zTreeStyle/img/diy/1_open.png");
		root.put("iconClose", "zTree_v3/css/zTreeStyle/img/diy/1_close.png");
		result.add(0, root);
		if (result == null || result.size() < 1) {
			return MsgReturn.ERROR_NODATA;
		}
		return new MsgReturn(MsgReturn.SUCCESS, PropertyCodeMsgUtil.getPropertyValue(MsgReturn.SUCCESS), result);
	}

	@Override
	public MsgReturn addCourseType(String name) throws SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("name", name);
		int result = courseMapping.addCourseType(name);
		if(result<1){
			return MsgReturn.FAIL_MSG;
		}
		return new MsgReturn(MsgReturn.SUCCESS, PropertyCodeMsgUtil.getPropertyValue(MsgReturn.SUCCESS), result);
	}

	@Override
	public MsgReturn addCategory(int pId, String name, String thumbnail, String vedioThumbnail, String fileId,
			String url, int sort, String introTitle, String introContext) throws SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pId", pId);
		params.put("name", name);
		params.put("thumbnail", thumbnail);
		params.put("vedioThumbnail", vedioThumbnail);
		params.put("fileId", fileId);
		params.put("url", url);
		params.put("sort", sort);
		params.put("introTitle", introTitle);
		params.put("introContext", introContext);
		int result = courseMapping.addCategory(pId, name, thumbnail, vedioThumbnail, fileId, url, sort, introTitle,
				introContext);
		if (result <= 0) {
			return MsgReturn.FAIL_MSG;
		}
		return new MsgReturn(MsgReturn.SUCCESS, PropertyCodeMsgUtil.getPropertyValue(MsgReturn.SUCCESS), result);
	}

	@Override
	public MsgReturn addSpecial(int pId, String name, String thumbnail, int sort) throws SQLException {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("pId", pId);
		params.put("name", name);
		params.put("thumbnail", thumbnail);
		params.put("sort", sort);
		int result = courseMapping.addSpecial(pId, name, thumbnail, sort);
		if (result <= 0) {
			return MsgReturn.FAIL_MSG;
		}
		return new MsgReturn(MsgReturn.SUCCESS, PropertyCodeMsgUtil.getPropertyValue(MsgReturn.SUCCESS), result);
	}

}
