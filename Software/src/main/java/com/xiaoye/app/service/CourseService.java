package com.xiaoye.app.service;

import java.sql.SQLException;

import com.xiaoye.app.common.MsgReturn;

public interface CourseService {
	/**
	 * 获取课程分类信息,分类信息,专题信息
	 * 
	 * @return
	 * @throws SQLException
	 */
	public MsgReturn getCourseList() throws SQLException;

	/**
	 * 添加分类
	 * 
	 * @param pId
	 * @param name
	 * @return
	 * @throws SQLException
	 */
	public MsgReturn addCourseType(String name) throws SQLException;

	/**
	 * 添加分类
	 * 
	 * @param pId
	 *            父类id
	 * @param name
	 *            标题title
	 * @param thumbnail
	 *            缩略图
	 * @param vedioThumbnail
	 *            视频缩略图
	 * @param fileId
	 *            文件ID
	 * @param url
	 *            视频播放网址
	 * @param sort
	 *            排序
	 * @param introTitle
	 *            简介标题
	 * @param introContext
	 *            简介正文
	 * @return
	 * @throws SQLException
	 */
	public MsgReturn addCategory(int pId, String name, String thumbnail, String vedioThumbnail, String fileId,
			String url, int sort, String introTitle, String introContext) throws SQLException;

	/**
	 * 
	 * @param pId
	 *            父类category id
	 * @param name
	 *            标题 title
	 * @param thumbnail
	 *            缩略图
	 * @param sort
	 *            排序,默认0
	 * @return
	 * @throws SQLException
	 */
	public MsgReturn addSpecial(int pId, String name, String thumbnail, int sort) throws SQLException;
}
