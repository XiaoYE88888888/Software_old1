package com.xiaoye.app.service;

import java.sql.SQLException;

import com.xiaoye.app.common.MsgReturn;
import com.xiaoye.app.entity.Page;
import com.xiaoye.app.entity.User;

public interface UserService {
	/**
	 * 登陆
	 * 
	 * @return
	 */
	public MsgReturn login(User user) throws SQLException;

	/**
	 * 查询用户列表
	 * 
	 * @param user
	 * @return
	 */
	public MsgReturn selectUserList(User user, Page page) throws SQLException;

	public MsgReturn selectUserListCount(User user, Page page) throws SQLException;

	/**
	 * 删除用户,更新is_delete=1
	 * 
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public MsgReturn deleteUser(User user) throws SQLException;

	/**
	 * 更新用户信息
	 * 
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public MsgReturn updateUser(User user) throws SQLException;
}
