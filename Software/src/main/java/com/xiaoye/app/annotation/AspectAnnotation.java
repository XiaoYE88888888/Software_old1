package com.xiaoye.app.annotation;

import java.lang.annotation.ElementType;  
import java.lang.annotation.Retention;  
import java.lang.annotation.RetentionPolicy;  
import java.lang.annotation.Target; 

@Target(ElementType.METHOD) // 这是一个对方法的注解，还可以是包、类、变量等很多东西  
@Retention(RetentionPolicy.RUNTIME) // 保留时间，一般注解就是为了框架开发时代替配置文件使用，JVM运行时
public @interface AspectAnnotation {
	
	String test() default "";
	
}
