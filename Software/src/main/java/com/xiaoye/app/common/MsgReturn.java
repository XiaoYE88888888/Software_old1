package com.xiaoye.app.common;

import com.xiaoye.app.util.PropertyCodeMsgUtil;

/**
 * 前后台返回数据用的桥接
 * 
 * @author xiaoye
 * @date 2018年5月3日
 * 
 * 
 */
public class MsgReturn {

	/**
	 * 成功/错误码
	 */
	private String code;
	/**
	 * 提示信息
	 */
	private Object message;
	/**
	 * 返回数据
	 */
	private Object data;
	// --------系统错误--------
	/**
	 * 系统繁忙
	 */
	public static String ERROR = "0";
	/**
	 * 参数错误
	 */
	public static String ERROR_PARAME = "1";
	/**
	 * 登录异常
	 */
	public static String LOAD_ERROR = "2";
	/**
	 * 登录失败
	 */
	public static String LOAD_EXCEPTION = "3";
	/**
	 * 无数据
	 */
	public static String NODATA = "4";

	// --------用户错误--------

	/**
	 * 验证码错误
	 */
	public static String VERIFICATION_CODE_ERROR = "10";
	/**
	 * 用户名不存在
	 */
	public static String USERNAME_NOT_EXIST = "11";
	/**
	 * 密码错误
	 */
	public static String PASSWOR_ERROR = "12";
	/**
	 * 手机号码长度有误
	 */
	public static String TEL_LENGTH_ERROR = "13";
	/**
	 * 请输入正确的手机号
	 */
	public static String TEL_IS_FALSE = "14";
	/**
	 * 邮箱格式有误
	 */
	public static String EMAIL_FORMAT_ERROR = "15";
	/**
	 * ---------------100 操作异常
	 */
	/**
	 * 添加失败
	 */
	public static String ADD_ERROR = "100";
	/**
	 * 删除失败
	 */
	public static String DELETE_ERROR = "101";
	/**
	 * 更新失败
	 */
	public static String UPDATE_ERROR = "102";
	/**
	 * 查询无结果
	 */
	public static String SELETE_ERROR = "103";
	/**
	 * 成功
	 */
	public static String SUCCESS = "200";
	/**
	 * 失败
	 */
	public static String FAIL = "201";
	/**
	 * ---------------300 异常 数据库异常
	 */
	public static String EXCEPTION_SQL = "300";

	private Integer totalCount;
	/**
	 * 系统繁忙
	 */
	public static MsgReturn ERROR_ERROR = new MsgReturn(ERROR, PropertyCodeMsgUtil.getPropertyValue(ERROR));
	/**
	 * 参数错误
	 */
	public static MsgReturn ERROR_PARAM = new MsgReturn(ERROR_PARAME,
			PropertyCodeMsgUtil.getPropertyValue(ERROR_PARAME));
	/**
	 * 无数据
	 */
	public static MsgReturn ERROR_NODATA = new MsgReturn(NODATA, PropertyCodeMsgUtil.getPropertyValue(NODATA));

	/**
	 * 操作成功
	 */
	public static MsgReturn SUCCESS_MSG = new MsgReturn(SUCCESS, PropertyCodeMsgUtil.getPropertyValue(SUCCESS));
	/**
	 * 操作失败
	 */
	public static MsgReturn FAIL_MSG = new MsgReturn(FAIL, PropertyCodeMsgUtil.getPropertyValue(FAIL));

	public MsgReturn(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public MsgReturn(String code, Object message, Object data) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
	}

	public MsgReturn(String code, Object message, Object data, Integer totalCount) {
		super();
		this.code = code;
		this.message = message;
		this.data = data;
		this.totalCount = totalCount;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public Object getdata() {
		return data;
	}

	public void setdata(Object data) {
		this.data = data;
	}

	public Integer getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(Integer totalCount) {
		this.totalCount = totalCount;
	}

	public void setMessage(Object message) {
		this.message = message;
	}

	@Override
	public String toString() {
		return "MsgReturn [code=" + code + ", message=" + message + ", data=" + data + ", totalCount=" + totalCount
				+ "]";
	}

}
