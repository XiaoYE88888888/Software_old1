package com.xiaoye.app.controller;

import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiaoye.app.common.MsgReturn;
import com.xiaoye.app.constant.passWordUtil;
import com.xiaoye.app.entity.Page;
import com.xiaoye.app.entity.User;
import com.xiaoye.app.service.UserService;
import com.xiaoye.app.util.PropertyCodeMsgUtil;
import com.xiaoye.app.util.StringUtil;

@Controller(value = "userController")
/**
 * 
 * @author xiaoye
 * @date 2018年5月3日
 * 
 *       此类的全局访问 "user/xxx.do"
 */
@RequestMapping(value = "user/")
public class UserController {
	@Autowired
	// @Qualifier("userService")
	private UserService userService;
	private static final Logger logger = Logger.getLogger(UserController.class);

	@RequestMapping(value = "login", method = RequestMethod.POST)
	@ResponseBody
	public MsgReturn login(User user) {
		if (user == null) {
			return MsgReturn.ERROR_PARAM;
		}
		String password = user.getPassword();
		if (StringUtil.STRING_IS_EMPTY(user.getUsername()) || StringUtil.STRING_IS_EMPTY(password)) {
			return MsgReturn.ERROR_PARAM;
		}
		user.setPassword(passWordUtil.md5ToRandomMd5(password));
		try {
			return userService.login(user);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info(PropertyCodeMsgUtil.getPropertyValue(MsgReturn.EXCEPTION_SQL), e);
		}
		return MsgReturn.ERROR_ERROR;
	}

	@RequestMapping(value = "selectUserList", method = RequestMethod.POST)
	@ResponseBody
	public MsgReturn selectUserList(User user, Page page) {
		try {
			page.setBegin();
			return userService.selectUserList(user, page);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info(PropertyCodeMsgUtil.getPropertyValue(MsgReturn.EXCEPTION_SQL), e);
		}
		return MsgReturn.ERROR_ERROR;
	}

	@RequestMapping(value = "deleteUser", method = RequestMethod.POST)
	@ResponseBody
	public MsgReturn deleteUser(User user) {
		if (user == null || StringUtil.INTEGER_IS_EMPTY(user.getId())) {
			return MsgReturn.ERROR_PARAM;
		}
		try {
			return userService.deleteUser(user);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info(PropertyCodeMsgUtil.getPropertyValue(MsgReturn.EXCEPTION_SQL), e);
		}
		return MsgReturn.ERROR_ERROR;
	}

	@RequestMapping(value = "updateUser", method = RequestMethod.POST)
	@ResponseBody
	public MsgReturn updateUser(User user) {
		if (user == null || StringUtil.INTEGER_IS_EMPTY(user.getId())) {
			return MsgReturn.ERROR_PARAM;
		}
		try {
			return userService.updateUser(user);
		} catch (SQLException e) {
			e.printStackTrace();
			logger.info(PropertyCodeMsgUtil.getPropertyValue(MsgReturn.EXCEPTION_SQL), e);
		}
		return MsgReturn.ERROR_PARAM;
	}
}
