package com.xiaoye.app.controller;

import java.sql.SQLException;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.xiaoye.app.common.MsgReturn;
import com.xiaoye.app.service.CourseService;
import com.xiaoye.app.util.StringUtil;

@Controller
@RequestMapping(value = "course/")
public class CourseController {
	private static Logger logger = LogManager.getLogger(CourseController.class);
	@Autowired
	private CourseService courseService;

	@RequestMapping(value = "getCourseList", method = RequestMethod.POST)
	@ResponseBody
	public MsgReturn getCourseList() {
		try {
			return courseService.getCourseList();
		} catch (SQLException e) {
			logger.info("", e);
			e.printStackTrace();
		}
		return MsgReturn.ERROR_NODATA;
	}

	/**
	 * 添加课程类型
	 * 
	 * @param pId
	 * @param name
	 * @return
	 */
	@RequestMapping(value = "addCourseType", method = RequestMethod.POST)
	@ResponseBody
	public MsgReturn addCourseType(String name) {
		if (StringUtil.STRING_IS_EMPTY(name)) {
			return MsgReturn.ERROR_PARAM;
		}
		try {
			return courseService.addCourseType(name);
		} catch (SQLException e) {
			logger.info("", e);
			e.printStackTrace();
		}
		return MsgReturn.ERROR_ERROR;

	}

	/**
	 * 添加课程种类
	 * 
	 * @param pId
	 *            父类id
	 * @param name
	 *            标题title
	 * @param thumbnail
	 *            缩略图
	 * @param vedioThumbnail
	 *            视频缩略图
	 * @param fileId
	 *            文件ID
	 * @param url
	 *            视频播放网址
	 * @param sort
	 *            排序
	 * @param introTitle
	 *            简介标题
	 * @param introContext
	 *            简介正文
	 * @return
	 */
	@RequestMapping(value = "addCategory", method = RequestMethod.POST)
	@ResponseBody
	public MsgReturn addCategory(Integer pId, String name, String thumbnail, String vedioThumbnail, String fileId,
			String url, Integer sort, String introTitle, String introContext) {
		if (StringUtil.INTEGER_IS_EMPTY(pId) || StringUtil.INTEGER_IS_EMPTY(sort) || StringUtil.STRING_IS_EMPTY(name)
				|| StringUtil.STRING_IS_EMPTY(thumbnail) || StringUtil.STRING_IS_EMPTY(vedioThumbnail)
				|| StringUtil.STRING_IS_EMPTY(fileId) || StringUtil.STRING_IS_EMPTY(url)
				|| StringUtil.STRING_IS_EMPTY(introTitle) || StringUtil.STRING_IS_EMPTY(introContext)) {
			return MsgReturn.ERROR_PARAM;
		}
		try {
			return courseService.addCategory(pId, name, thumbnail, vedioThumbnail, fileId, url, sort, introTitle,
					introContext);
		} catch (SQLException e) {
			logger.info("", e);
			e.printStackTrace();
		}
		return MsgReturn.ERROR_ERROR;
	}

	public MsgReturn addSpecial(Integer pId, String name, String thumbnail, Integer sort) {
		if (StringUtil.INTEGER_IS_EMPTY(pId) || StringUtil.INTEGER_IS_EMPTY(sort) || StringUtil.STRING_IS_EMPTY(name)
				|| StringUtil.STRING_IS_EMPTY(thumbnail)) {
			return MsgReturn.ERROR_PARAM;
		}
		try {
			return courseService.addSpecial(pId, name, thumbnail, sort);
		} catch (SQLException e) {
			logger.info("", e);
			e.printStackTrace();
		}
		return MsgReturn.ERROR_ERROR;
	}
}
