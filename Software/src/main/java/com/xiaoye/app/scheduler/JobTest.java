package com.xiaoye.app.scheduler;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.xiaoye.app.entity.Page;
import com.xiaoye.app.entity.User;
import com.xiaoye.app.service.UserService;

/**
 * 
 * @author xiaoye
 * @date 2018年5月23日
 * 
 *       定时任务测试
 *
 */
@Component
public class JobTest {
	private static final Logger logger = LogManager.getLogger(JobTest.class);
	@Autowired
	private UserService userService;

	public void getUser() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		logger.info("定时任务: " + simpleDateFormat.format(new Date()));
	}

	public void onTimeMethod() {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		logger.info("几点执行任务: " + simpleDateFormat.format(new Date()));
	}

	public void getUserList() {
		try {
			System.out.println(userService.selectUserList(new User(), new Page()));
		} catch (SQLException e) {
			logger.info("获取数据失败", e);
			e.printStackTrace();
		}
	}
	/*
	 * public static void main(String[] args) { AbstractApplicationContext
	 * abstractApplicationContext = new ClassPathXmlApplicationContext(
	 * "quartz-context.xml"); }
	 */
}
