package com.xiaoye.app.entity;

/**
 * 登录用户实体类
 * 
 * @author xiaoye
 * @String 2018年5月3日
 * 
 * 
 *
 */
public class User {
	private Integer id;
	private String name;
	private String username;
	private String password;
	private String token;
	private String tel;
	/**
	 * 角色权限ID
	 */
	private Integer role;
	private String qqNumber;
	private String weChatNumber;
	/**
	 * 缩略图
	 */
	private String headPortrait;
	private String createDate;
	private String modifyDate;
	/**
	 * 是否删除;0:默认未删除;1:已删除;
	 */
	private Integer isDelete;

	public User() {
	}

	public User(Integer id, String name, String username, String password, String token, String tel, Integer role,
			String qqNumber, String weChatNumber, String headPortrait, String createDate, String modifyDate, Integer isDelete) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.password = password;
		this.token = token;
		this.tel = tel;
		this.role = role;
		this.qqNumber = qqNumber;
		this.weChatNumber = weChatNumber;
		this.headPortrait = headPortrait;
		this.createDate = createDate;
		this.modifyDate = modifyDate;
		this.isDelete = isDelete;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getTel() {
		return tel;
	}

	public void setTel(String tel) {
		this.tel = tel;
	}

	public Integer getRole() {
		return role;
	}

	public void setRole(Integer role) {
		this.role = role;
	}

	public String getQqNumber() {
		return qqNumber;
	}

	public void setQqNumber(String qqNumber) {
		this.qqNumber = qqNumber;
	}

	public String getWeChatNumber() {
		return weChatNumber;
	}

	public void setWeChatNumber(String weChatNumber) {
		this.weChatNumber = weChatNumber;
	}

	public String getHeadPortrait() {
		return headPortrait;
	}

	public void setHeadPortrait(String headPortrait) {
		this.headPortrait = headPortrait;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(String modifyDate) {
		this.modifyDate = modifyDate;
	}

	public Integer getIsDelete() {
		return isDelete;
	}

	public void setIsDelete(Integer isDelete) {
		this.isDelete = isDelete;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", username=" + username + ", password=" + password + ", token="
				+ token + ", tel=" + tel + ", role=" + role + ", qqNumber=" + qqNumber + ", weChatNumber="
				+ weChatNumber + ", headPortrait=" + headPortrait + ", createDate=" + createDate + ", modifyDate="
				+ modifyDate + ", isDelete=" + isDelete + "]";
	}

}
