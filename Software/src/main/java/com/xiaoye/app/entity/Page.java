package com.xiaoye.app.entity;

/**
 * 页数实体
 * 
 * @author xiaoye
 * @date 2018年5月16日
 * 
 * 
 *
 */
public class Page {
	/**
	 * 页数
	 */
	private Integer page;
	/**
	 * 显示行数
	 */
	private Integer limit;

	private Integer begin;
	/**
	 * 总记录数
	 */
	private Integer totalcount;

	public Page() {
		super();
	}

	public Page(Integer page, Integer limit, Integer totalcount) {
		super();
		this.page = page;
		this.limit = limit;
		this.totalcount = totalcount;
	}

	public Integer getPage() {
		return page;
	}

	public void setPage(Integer page) {
		this.page = page;
	}

	public Integer getLimit() {
		return limit;
	}

	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public Integer getBegin() {
		return begin;
	}

	public void setBegin() {
		this.begin = (page - 1) * limit;
	}

	public Integer getTotalcount() {
		return totalcount;
	}

	public void setTotalcount(Integer totalcount) {
		this.totalcount = totalcount;
	}

	@Override
	public String toString() {
		return "Page [page=" + page + ", limit=" + limit + ", begin=" + begin + ", totalcount=" + totalcount + "]";
	}

	

}