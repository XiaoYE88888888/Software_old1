package com.xiaoye.app.constant;

import com.xiaoye.app.common.MD5Util;
import com.xiaoye.app.util.StringUtil;

/**
 * 
 * @author xiaoye
 * @date 2018年5月10日
 * 
 *       password 操作
 *
 */
public class passWordUtil {
	/**
	 * 全局默认密码
	 */
	public final static String DEFAULT_PWD = "123456";

	/**
	 * 获取密码MD5值;
	 * 
	 * 参数为null时, 则加密并返回默认密码的MD5值;
	 * 
	 * @return
	 */
	public static String createMD5Pwd(String password) {
		if (StringUtil.STRING_IS_EMPTY(password)) {
			return MD5Util.MD5(DEFAULT_PWD);

		}
		return MD5Util.MD5(password.toUpperCase());
	}

	/**
	 * 传入MD5值,取两次随机数,继续加密
	 * 
	 * @param password
	 * @return
	 */
	public static String md5ToRandomMd5(String password) {
		if (StringUtil.STRING_IS_EMPTY(password)) {
			return MD5Util.MD5(MD5Util.MD5(DEFAULT_PWD));
		}
		password = password.toUpperCase();
		password += password.substring(2, 4);
		password += password.substring(8, 12);
		return MD5Util.MD5(password);

	}

	/**
	 * 密码MD5加密一次,去两次随机数,继续加密一次;
	 * 
	 * 参数为null时, 则加密并返回默认密码的MD5值;
	 * 
	 * @return
	 */
	public static String doubleMD5PWD(String password) {
		if (StringUtil.STRING_IS_EMPTY(password)) {
			return MD5Util.MD5(MD5Util.MD5(DEFAULT_PWD));
		}
		password = MD5Util.MD5(password.toUpperCase());
		password += password.substring(2, 4);
		password += password.substring(8, 12);
		return MD5Util.MD5(password);
	}

	public static void main(String[] args) {
		// 大小写加密后不一样
		System.out.println("md5加密:" + createMD5Pwd("      "));
		System.out.println("123456 - md5加密:" + createMD5Pwd("123456"));
		System.out.println("system - md5加密:" + createMD5Pwd("system"));
		System.out.println("md5加密后取随机md5加密:" + md5ToRandomMd5("54b53072540eeeb8f8e9343e71f28176"));
		System.out.println("md5加密后取随机md5加密:" + md5ToRandomMd5("54B53072540EEEB8F8E9343E71F28176"));
		System.out.println("两次md5加密:" + doubleMD5PWD("system"));

	}
}
