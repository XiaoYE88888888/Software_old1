package com.xiaoye.app.mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

import com.xiaoye.app.entity.User;

/**
 * 
 * @author xiaoye
 * @date 2018年5月14日
 * 
 *       用户持久层
 *
 */
public interface UserMapper {
	/**
	 * 查询用户信息
	 * 
	 * @param user
	 * @return
	 */
	public List<User> selectUserList(Map params) throws SQLException;

	public int selectUserListCount(Map params) throws SQLException;

	/**
	 * 登陸
	 * 
	 * @param user
	 * @return
	 */
	public User login(User user) throws SQLException;
	/**
	 * 删除用户,更新is_delete = 1
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public int deleteUser(User user)throws SQLException;
	/**
	 * 更新用户信息
	 * 
	 * @param user
	 * @return
	 * @throws SQLException
	 */
	public int updateUser(User user) throws SQLException;
}
