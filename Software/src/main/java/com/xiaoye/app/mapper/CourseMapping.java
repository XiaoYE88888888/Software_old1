package com.xiaoye.app.mapper;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;

public interface CourseMapping {
	/**
	 * 获取课程信息
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<Map<String, Object>> getCourseList() throws SQLException;

	/**
	 * 添加课程类型
	 * 
	 * @param pId
	 * @param name
	 * @return
	 * @throws SQLException
	 */
	public int addCourseType(String name) throws SQLException;

	/**
	 * 添加分类
	 * 
	 * @param pId
	 * @param name
	 * @return
	 * @throws SQLException
	 */
	public int addCategory(int pId, String name, String thumbnail, String vedioThumbnail, String fileId, String url,
			int sort, String introTitle, String introContext) throws SQLException;

	/**
	 * 添加专题分类
	 * 
	 * @param pId
	 * @param name
	 * @return
	 * @throws SQLException
	 */
	public int addSpecial(int pId, String name, String thumbnail, int sort) throws SQLException;
}
