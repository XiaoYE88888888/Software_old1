package com.xiaoye.app.aop;

import java.lang.reflect.Method;

import org.springframework.cglib.proxy.InvocationHandler;
import org.springframework.cglib.proxy.Proxy;

public class MyProxy implements InvocationHandler {
	/**
	 * 要代理的目标对象
	 */
	private Object target;

	/**
	 * 私有构造器,禁止new本类对象
	 */
	private MyProxy() {
	};

	/**
	 * 返回一个动态代理对象
	 * 
	 * @param object
	 * @return
	 */
	public static Object newInstance(Object object) {
		MyProxy myProxy = new MyProxy();
		myProxy.target = object;
		// 通过Proxy的newProxyInstance来获取代理对象
		/**
		 * 第一个参数:获取目标对象的加载器
		 * 
		 * 第二个参数:获取目标对象的接口
		 * 
		 * 
		 * 第三个参数:设置回调对象,当前代理对象的方法被调用时,会委派该参数去调用本类的invoke方法;
		 */
		Object resultObject = Proxy.newProxyInstance(object.getClass().getClassLoader(),
				object.getClass().getInterfaces(), myProxy);
		return resultObject;
	}

	@Override
	public Object invoke(Object arg0, Method arg1, Object[] arg2) throws Throwable {
		// TODO Auto-generated method stub
		return null;
	}

}
