package com.xiaoye.app.aop;

import org.aspectj.lang.annotation.*;

/**
 * 切面类
 */
@Aspect
public class MyAspect {

    @Pointcut("execution(* com.xiaoye.app.service..*.*(..))")
    public void pointcutMethod() {

    }


    @Before("pointcutMethod()")
    public void beforeAdvice() {
        System.out.println("前置通知~~~~~~~~~~~~~~");
    }

    @After("pointcutMethod()")
    public void afterAdvice() {
        System.out.println("后置通知~~~~~~~~~~~~~~");
    }

    @Around("pointcutMethod()")
    public void aroundAdvice() {
        System.out.println("环绕通知~~~~~~~~~~~~~~~");
    }

    @AfterReturning("pointcutMethod()")
    public void afterReturnAdvice() {
        System.out.println("方法返回后通知~~~~~~~~~~");
    }

    @AfterThrowing("pointcutMethod()")
    public void afterException() {
        System.out.println("抛出异常后通知~~~~~~~~~~");
    }
}
