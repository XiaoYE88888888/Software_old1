package com.xiaoye.app.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * ���ڴ�����
 * 
 * @author xiaoye
 * @date 2018��4��8��
 * 
 * 
 * 
 */
public class DateUtil {
	private static Logger logger = LogManager.getLogger(DateUtil.class);
	private static SimpleDateFormat sdfYMD;
	private static SimpleDateFormat sdfYMDHDS;

	static {
		sdfYMD = new SimpleDateFormat("yyyy-MM-dd");
		sdfYMDHDS = new SimpleDateFormat("yyyy-MM-dd HH:DD:ss");
	}

	/**
	 * Date 转换成 yyyy-MM-dd
	 * 
	 * @param date
	 * @return
	 */
	public static String DateToStringYMD(Date date) {
		if (date == null) {
			return "日期不能为null";
		}
		return sdfYMD.format(date);
	}

	public static String DateToStringYMDHDS(Date date) {
		if (date == null) {
			return "日期不能为null";
		}
		return sdfYMDHDS.format(date);
	}

	public static Date StringToDateYMD(String str) {
		if (StringUtil.STRING_IS_EMPTY(str)) {
			return null;
		}
		try {
			return sdfYMD.parse(str);
		} catch (ParseException e) {
			logger.info(str + ",字符转换日期错误", e);
			e.printStackTrace();
		}
		return null;
	}

	public static Date StringToDateYMDHDS(String str) {
		if (StringUtil.STRING_IS_EMPTY(str)) {
			return null;
		}
		try {
			return sdfYMDHDS.parse(str);
		} catch (ParseException e) {
			logger.info(str + ",字符转换日期错误", e);
			e.printStackTrace();
		}
		return null;
	}
}
