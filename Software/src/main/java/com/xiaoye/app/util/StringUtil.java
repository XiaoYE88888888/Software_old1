package com.xiaoye.app.util;

/**
 * 字符串工具类
 * 
 * @author xiaoye
 * @date 2018-4-8
 */
public class StringUtil {
	/**
	 *
	 * 字符串是否为null,为空,空串;
	 * 
	 * @param string
	 * @return null||空串:true;否则:false;
	 */
	public static boolean STRING_IS_EMPTY(String string) {
		if (string == null || string.trim().equals("")) {
			return true;
		}
		return false;
	}

	/**
	 * 整型是否为null或者小于等于0
	 * 
	 * @param integer
	 * @return null||integer <= 0:true ; false;
	 */
	public static boolean INTEGER_IS_EMPTY(Integer integer) {
		if (integer == null || integer < 0) {
			return true;
		}
		return false;
	}

	public static void main(String[] args) {
		System.out.println(STRING_IS_EMPTY(""));
	}
}
