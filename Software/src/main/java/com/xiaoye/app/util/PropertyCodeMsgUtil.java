package com.xiaoye.app.util;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.log4j.Logger;

/**
 * 成功/错误码配置文件的加载
 * 
 * @author xiaoye
 * @date 2018年5月3日
 * 
 * 
 *
 */
public class PropertyCodeMsgUtil {
	private static final Logger logger = Logger.getLogger(PropertyCodeMsgUtil.class);
	private static Properties properties;

	static {
		loadProperty();
	}

	synchronized static void loadProperty() {
		logger.info("--------开始加载CodeMsg配置文件--------");
		properties = new Properties();
		InputStreamReader inputStream = null;
		try {
			inputStream = new InputStreamReader(
					PropertyCodeMsgUtil.class.getClassLoader().getResourceAsStream("codeMsg.properties"), "UTF-8");
			properties.load(inputStream);
		} catch (IOException e) {
			logger.info("codeMsg.properties文件加载失败!", e);
		} finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				} catch (IOException e) {
					logger.info("关闭文件流异常", e);
				}
			}
		}
		logger.info("--------CodeMsg配置文件加载结束--------");
	}

	public static String getPropertyValue(String key) {
		if (properties == null) {
			loadProperty();
		}
		return properties.getProperty(key);

	}
}
