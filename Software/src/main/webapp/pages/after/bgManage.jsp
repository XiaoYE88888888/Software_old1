<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<title>萧曵管理后台</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="js/layui-v2.2.6/layui/css/layui.css">
<link>
<style type="text/css">
<
style type ="text /css">* {
	margin: 0px;
	padding: 0px;
}

.user-photo {
	width: 200px;
	height: 130px;
	padding: 15px 0 5px;
}

.user-photo a.img {
	display: block;
	width: 80px;
	height: 80px;
	margin: 0 auto 10px;
}

.user-photo a.img img {
	display: block;
	border: none;
	width: 100%;
	height: 100%;
	border-radius: 50%;
	-webkit-border-radius: 50%;
	-moz-border-radius: 50%;
	border: 4px solid #44576b;
	box-sizing: border-box;
}

.user-photo p {
	display: block;
	width: 100%;
	height: 25px;
	color: #ffffff;
	text-align: center;
	font-size: 12px;
	white-space: nowrap;
	line-height: 25px;
	overflow: hidden;
}
</style>
<script type="text/javascript" src="js/layui-v2.2.6/layui/layui.all.js"></script>
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>

</head>

<body class="layui-layout-body">
	<div class="layui-layout layui-layout-admin">
		<div class="layui-header">
			<div class="layui-logo"><font size="4px" >萧曵管理后台</font></div>
			<!-- 头部区域（可配合layui已有的水平导航） -->
			<ul class="layui-nav layui-layout-left">
			  <li class="layui-nav-item layadmin-flexible kit-side-fold" lay-unselect="">
	            <a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
	              <i id ="xy" class="layui-icon layui-icon-shrink-right" style="font-size: 17px; color: #FFF;"></i>
	            </a>
	          </li>
						<li class="layui-nav-item">
							<a  href="pages/after/userManage.jsp" target="myIframe"> <i class="layui-icon" style="font-size: 17px; color: #FFF;">&#xe68e;</i>&nbsp;&nbsp;
							</a>
						</li>
	          <li class="layui-nav-item" lay-unselect="">
	            <a href="javascript:;" class="refresh refreshThis" layadmin-event="refresh" title="刷新">
	              <i class="layui-icon layui-icon-refresh-3" style="font-size: 17px; color: #FFF;"></i>
	            </a>
	          </li>
	        <span class="layui-nav-bar" style="left: 38px; top: 48px; width: 0px; opacity: 0;"></span>
        </ul>
			<ul class="layui-nav layui-layout-right">
				<li class="layui-nav-item" style=" margin-right:20px;">
			  		 	<i class="layui-icon layui-icon-snowflake" style="font-size: 20px; color: #FFF;"></i>
					<dl class="layui-nav-child layui-anim layui-anim-upbit">
						<iframe allowtransparency="true" frameborder="0" width="140" height="428" scrolling="no" src="//tianqi.2345.com/plugin/widget/index.htm?s=1&z=1&t=0&v=1&d=5&bd=0&k=000000&f=000000&ltf=80ff00&htf=800000&q=1&e=1&a=1&c=54511&w=140&h=428&align=center"></iframe>
					</dl>
			 	 </li>
				<li class="layui-nav-item" id="nowTime">
				</li>
				<li class="layui-nav-item">
					<a href="${pageContext.request.contextPath}/rest/index/exit.do">
						<i style="font-size: 16px; color: #FFF;">EXIT</i>
					</a>
				</li>
			</ul>
		</div>

		<div class="layui-side layui-bg-black">
			<div class="layui-side-scroll">
				<div class="user-photo">
					<a class="img" title="我的头像" href="accountInfo.jsp" target="myIframe"><img  id="image" src="image/头像.jpg" ></a>
					<p>
						<span id="userName" class="userName"></span><span id="userCompany" class="userName"></span>
					</p>
				</div>
				<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
				<ul class="layui-nav layui-nav-tree" lay-filter="demo" id="leftNav">
					<li class="layui-nav-item">
						<a><i class="layui-icon"></i><span>用户管理</span></a>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>用户查询</span></a></dl>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>学员管理</span></a></dl>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>管理员管理</span></a></dl>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>部门管理</span></a>
							<dd></dd>
						</dl>
					</li>
					<li class="layui-nav-item">
						<a><i class="layui-icon"></i><span>公告管理</span></a>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>系统公告</span></a></dl>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>推送信息</span></a></dl>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>管理员公告</span></a></dl>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>公告</span></a></dl>
					</li>
					<li class="layui-nav-item">
						<a><i class="layui-icon"></i><span>数据中心</span></a>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>数据使用情况</span></a></dl>
					</li>
					<li class="layui-nav-item">
						<a><i class="layui-icon"></i><span>个人中心</span></a>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>资料修改</span></a></dl>
					</li>
					<li class="layui-nav-item">
						<a><i class="layui-icon"></i><span>反馈管理</span></a>
						<dl class="layui-nav-child"><a href="javascript:void(0);" target="myIframe"><span>问题反馈列表</span></a></dl>
					</li>
				</ul>
				
			</div>
		</div>
		<!-- 内容主体区域 -->
		<div id = "mainArea" class="layui-body layui-form" style="margin-left: 20px;">
			<div style="height:100%;width:100%;margin-bottom: 200px;">
					<iframe  name="myIframe" width="100%" height="100%"
						src="pages/after/userManage.jsp" frameborder="0" seamless scrolling="no" ></iframe>
			</div>
		</div>
			<div class="layui-footer">
				<div>
					<img alt="图片加载失败" src="image/haima_black.png" style="position: relative; bottom: 90px;left: 35%;">
				</div>
			</div>
		</div>
		

</body>
<script type="text/javascript">
	getUserMessage();
	<%-- 一级菜单单击后的样式 --%>
	$("#leftNav").children("li").click(function(){
	$("#leftNav").children("li").removeClass("layui-nav-itemed");
	$(this).addClass("layui-nav-itemed");
	if(!isShow){
			shouSuo();
		}
	});
	<%-- 伸缩控制 --%>
	$('.kit-side-fold').click(function(){
    	shouSuo();
    });
	$(document).keypress(function(e) {
	       var eCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
	        if (eCode == 96){
	            shouSuo();
	        }
		});
	$(window).resize(function() {  
		var width = $(this).width();  
		 if(width < 950 && isShow == true){
		 	shouSuo();
	  	}else if(width >= 950 && isShow == false){
		 	shouSuo();
	  	}
	});  
	
	//刷新当前
	$(".refresh").on("click",function(){  //此处添加禁止连续点击刷新一是为了降低服务器压力，另外一个就是为了防止超快点击造成chrome本身的一些js文件的报错(不过貌似这个问题还是存在，不过概率小了很多)
		if($(this).hasClass("refreshThis")){
			$(this).removeClass("refreshThis");
			$("iframe[name='myIframe']")[0].contentWindow.location.reload(true);
			setTimeout(function(){
				$(".refresh").addClass("refreshThis");
			},2000)
		}else{
			layer.msg("您还是等两秒再刷新吧！");
		}
	});
	<%-- 获取时间 --%>
	function showTime(){
		var week=new Date()
		var weekday=new Array(7)
		weekday[0]="周日"
		weekday[1]="周一"
		weekday[2]="周二"
		weekday[3]="周三"
		weekday[4]="周四"
		weekday[5]="周五"
		weekday[6]="周六"
	    nowtime=new Date();
	    year=nowtime.getFullYear();
	    month=nowtime.getMonth()+1;
	    date=nowtime.getDate();
	    week=nowtime.getDay();
	    document.getElementById("nowTime").innerText=year+"年"+month+"月"+date+"   "+weekday[week]+"   "+nowtime.toLocaleTimeString();
	}

	setInterval(showTime,500);
	
	
layui.use(['layer', 'laydate'], function(){ 
  var layer = layui.layer // 获取layer组件
  ,laydate = layui.laydate; // 获取laydate组件
});
var isShow = true;  //定义一个标志位
<%-- 登陆以后获取用户信息 --%>
function getUserMessage(){
	$.ajax({
					url : "${pageContext.request.contextPath}/rest/common/getInfo.do",
					type : 'get',
					cache : false,
					success : function(data) {
						if (data.code == 100) {
							data = data.data;
							$("#userName").html(data.name);
							$("#userCompany").html("("+data.company+")");
							var image = data.image;
							if (image != null && image.length > 0) {
								$('#image').attr('src', image);
							}
	
						} else {
							layer.msg(data.data, {
								time : 1000
							});
						}
					}
				});
	
}
layui.use('element', function() {
			var element = layui.element;
	
		});

<%--控制左侧功能菜单的收缩 --%>
function shouSuo(){
		$("#xy").toggleClass("layui-icon-spread-left");
        $("#xy").toggleClass("layui-icon-shrink-right");
	//选择出所有的span，并判断是不是hidden
	$('.layui-nav-item span').each(function(){
	            if($(this).is(':hidden')){
	                $(this).show();
	            }else{
	            	$(".layui-nav-item").removeClass("layui-nav-itemed");
	                $(this).hide();
	            }
	        });
	 //判断isshow的状态
        if(isShow){
            $('.layui-side.layui-bg-black').width(60); //设置宽度
            $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
            //将footer和body的宽度修改
            $('.layui-body').css('left', 60+'px');
            $('.layui-footer').css('left', 60+'px');
            //将二级导航栏隐藏
            $('dd span').each(function(){
                $(this).hide();
            });
            //修改标志位
            isShow =false;
        }else{
            $('.layui-side.layui-bg-black').width(200);
            $('.kit-side-fold i').css('margin-right', '10%');
            $('.layui-body').css('left', 200+'px');
            $('.layui-footer').css('left', 200+'px');
            $('dd span').each(function(){
                $(this).show();
            });
            isShow =true;
        }
}
	</script>
</html>
