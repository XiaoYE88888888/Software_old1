<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<title>用户信息表</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="js/layui-v2.2.6/layui/css/layui.css" media="all">
</head>
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/layui-v2.2.6/layui/layui.all.js"></script>
<script type="text/html" id="barDemo">
	<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="detail">查看</a>
  	<a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
  	<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<body>
	<fieldset class="layui-elem-field layui-field-title"
		style="margin-top: 20px;">
		<legend>默认表格</legend>
	</fieldset>
	<div>
		<table class="layui-hide" id="userList" lay-filter="userList"></table>
	</div>
</body>
<script type="text/javascript">
		layui.use(['laydate', 'laypage', 'layer', 'table', 'carousel', 'upload', 'element'], function(){
			  var laydate = layui.laydate //日期
			  ,laypage = layui.laypage //分页
			  ,layer = layui.layer //弹层
			  ,table = layui.table //表格
			  ,carousel = layui.carousel //轮播
			  ,upload = layui.upload //上传
			  ,element = layui.element; //元素操作
			  //监听Tab切换
			  element.on('tab(demo)', function(data){
			    layer.msg('切换了：'+ this.innerHTML);
			    console.log(data);
			  });
		   //执行一个 table 实例
			  table.render({
			    elem: '#userList'
			    ,id: 'userListReload'
			    ,width: 'full'//撑满宽度
    			,height: 400
    			,async: false //是否异步
			   	,url: 'user/selectUserList.do' //数据接口
			 	,method: 'POST'
			    ,cellMinWidth: 200 //全局定义常规单元格的最小宽度，layui 2.2.1 新增,设置以后列默认值就是这个,如果表头里面也写了宽度,则以表头为主
			    ,cols: [[ //表头
			       {type:'checkbox', fixed: 'left'}//固定在左
			      ,{field: 'id', title: 'ID', width:80, sort: true, fixed: 'left'}
			      ,{field: 'name', title: '用户名',style:'background-color: #009688; color: #fff;'}
			      ,{field: 'username', title: '账号', sort: true}
			      ,{field: 'tel', title: '电话',edit:'text'} //是否可以编辑,目前只支持type="text"的input编辑。
			      ,{field: 'qqNumber', title: 'QQ',edit:'text'}
			      ,{field: 'weChatNumber', title: '微信',edit:'text', sort: true}
			      ,{field: 'role', title: 'role',  sort: true}
			      ,{field: 'createDate', title: '创建时间'}
			      ,{field: 'isDelete', title: '是否删除', sort: true}
			      ,{fixed: 'right', title:'操作' , align:'center', toolbar: '#barDemo'}
			    ]]
			    ,page:{ //支持传入 laypage 组件的所有参数（某些参数除外，如：jump/elem） - 详见文档
					      layout: ['limit','limits', 'count', 'prev', 'page', 'next', 'skip'] //自定义分页布局
					      ,curr: 1 //设定初始在第1 页
					      ,groups: 5 //只显示 5 个连续页码
					      ,first: true //显示首页
					      ,last: true //显示尾页
					      ,limit: 5 //每页显示的条数。laypage将会借助 count 和 limit 计算出分页数
					      ,limits: [5,10,15,20] //每页条数的选择项。如果 layout 参数开启了 limit，则会出现每页条数的select选择框
					      
			    }
			    /* ,limit: 2
			    ,limits: [2,3,4]
			    ,count: 'totalCount' */
			    ,where : {
			    //传值 startDate : startDate,
				}
			    /* ,request: {//用于对分页请求的参数：page、limit重新设定名称，如下   ;那么请求数据时的参数将会变为：?curr=1&nums=30
			    	 pageName: 'pages' //页码的参数名称，默认：page
			    	,limitName: 'counts' //每页数据量的参数名，默认：limit
			    } */
				,response: {
					  statusName: 'code' //数据状态的字段名称，默认：code
					  ,statusCode: 200 //成功的状态码，默认：0
					  ,msgName: 'message' //状态信息的字段名称，默认：msg
					  ,countName: 'totalCount' //数据总数的字段名称，默认：count
					  ,dataName: 'data' //数据列表的字段名称，默认：data
					}
				,text: { //自定义文本，如空数据时的异常提示等。注：layui 2.2.5 开始新增。
					    none: '暂无相关数据' //默认：无数据。注：该属性为 layui 2.2.5 开始新增
					  }
				,done: function(res, curr, count){
				    //如果是异步请求数据方式，res即为你接口返回的信息。
				    //如果是直接赋值的方式，res即为：{data: [], count: 99} data为当前页数据、count为数据总长度
				    //console.log(res.data);
				    	
				    //得到当前页码
				   // console.log(curr); 
				    
				    //得到数据总量
				    //console.log(count);
				  }   
			  });
			  
			   //监听工具条
			  table.on('tool(userList)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
			  console.log(obj)
			    var data = obj.data; //获得当前行数据
			   
			    var layEvent = obj.event; //获得 lay-event 对应的值
			     var tr = obj.tr; //获得当前行 tr 的DOM对象
			    if(layEvent === 'detail'){
			      layer.msg('查看操作');
			    } else if(layEvent === 'del'){
			      layer.confirm('真的删除行么', function(index){
			        //向服务端发送删除指令
			        $.ajax({
			        	url:"${pageContext.request.contextPath}/user/deleteUser.do",
			        	type:'post',
			        	cache:'false',
			        	data:{
			        		'id':data.id
			        	},
			        	success:function(data){
			        		if(data.code != 200){
			        			layer.msg(data.message);
			        		}else{
			        			layer.msg(data.message);
			        			table.reload("userListReload"); //表格刷新,值为table 的id 值
			        		}
			        	}
			        })
			      });
			    } else if(layEvent === 'edit'){
			      layer.msg('编辑操作');
			    }
			  });
			   //监听单元格编辑
            table.on('edit(userList)', function (obj) {
            	var data = obj.data;//获取当前行对象
                var value = obj.value; //获取修改后的值
			    var field = obj.field;//得到字段
			    layer.msg("ID:" + data.id + "  ,field:" + field + " ,value:" + value);
			 	   $.ajax({
			    	url:"${pageContext.request.contextPath}/user/updateUser.do",
			    	type:"post",
			    	data:{
			    		"id" : data.id ,
			    		"tel" : data.tel,
			    		"qqNumber" : data.qqNumber,
			    		"weChatNumber" : data.weChatNumber,
			    	},
			    	success:function(data){
			    		if(data.code != 200){
			    			layer.msg(data.message);
			    		}else{
			    			layer.msg(data.message);
			    			table.reload("userListReload"); //表格刷新,值为table 的id 值
			    		}
			    	}
			    })
            });
		});
 
</script>
</html>
