<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>My JSP 'courseNew.jsp' starting page</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="js/layui-v2.2.6/layui/css/layui.css" media="all">
<link rel="stylesheet" type="text/css"
	href="zTree_v3/css/zTreeStyle/zTreeStyle.css" media="all">
<link rel="stylesheet" type="text/css"
	href="zTree_v3/css/metroStyle/metroStyle.css" media="all">
<style type="text/css">
</style>
</head>
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js	/layui-v2.2.6/layui/layui.all.js"></script>
<script type="text/javascript" src="zTree_v3/js/jquery.ztree.all.min.js"></script>
<body>
	<div class="zTreeDemoBackground left">
		<ul id="treeDemo" class="ztree" style="width:500px; overflow:auto;"></ul>
	</div>

</body>
<script type="text/javascript">
	var zTree;
	var treeNodes;

	$(function() {
		$.ajax({
			async : false,
			cache : false,
			type : 'POST',
			dataType : "json",
			url : "course/getCourseList.do", //请求的action路径
			error : function() { //请求失败处理函数
				alert('请求失败');
			},
			success : function(data) { //请求成功后处理函数。  
				//alert(data.data);
				treeNodes = data.data; //把后台封装好的简单Json格式赋给treeNodes
			}
		});

		$.fn.zTree.init($("#treeDemo"), setting, treeNodes);
	});

	var setting = {
		treeNodeKey : "id", //在isSimpleData格式下，当前节点id属性
		treeNodeParentKey : "pId", //在isSimpleData格式下，当前节点的父节点id属性
		showLine : true, //是否显示节点间的连线
		checkable : false, //每个节点上是否显示 CheckBox
		view : {
			addHoverDom : addHoverDom, //当鼠标移动到节点上时，显示用户自定义控件  
			removeHoverDom : removeHoverDom, //离开节点时的操作

		},
		data : {
			key : {
				title : "name", //鼠标悬停显示的信息
				name : "name", //网页上显示出节点的名称
				iconOpen : "iconOpen ",
				iconClose : "iconClose  ",
			},
			simpleData : {
				enable : true,
				idKey : "id", //修改默认的ID为自己的ID
				pIdKey : "pId", //修改默认父级ID为自己数据的父级ID
				rootPId : 0 //根节点的ID
			}
		},
		edit : {
			enable : true,
			addNodes : true,
			showRemoveBtn : true, //表示 显示 / 隐藏 删除按钮1、首先触发 setting.callback.beforeRemove 回调函数，用户可判定是否进行删除操作。2、如果未设置 beforeRemove 或 beforeRemove 返回 true，则删除节点并触发
			showRenameBtn : true,
		},
		callback : { //回调函数
			//rightClick : zTreeOnRightClick   //右键事件
			beforeRename : renameNote //重命名
		}
	}
	// isCancel:是否取消操作,true取消
	function renameNote(treeId, treeNode, newName, isCancel) {
		if (isCancel) {
			return;
		}
		layer.msg("treeId:" + treeId + "newName:" + newName + "isCancel:" + isCancel);
	}
	//增加节点
	function addHoverDom(treeId, treeNode) {
		console.log("treeId=" + treeId + " ,treeNode.id=" + treeNode.id + "  ,treeNode.name=" + treeNode.name + ",  treeNode.tid=" + treeNode.tid);
		if (treeNode.id == null || treeNode.id == "undefined") {
			layer.msg("请重新操作");
			return false;
		}

		var sObj = $("#" + treeNode.tId + "_span"); //获取节点信息  
		if (treeNode.editNameFlag || $("#addBtn_" + treeNode.tId).length > 0) return;

		var addStr = "<span class='button add' id='addBtn_" + treeNode.tId + "' title='add node' onfocus='this.blur();'></span>"; //定义添加按钮  
		sObj.after(addStr); //加载添加按钮  
		var btn = $("#addBtn_" + treeNode.tId);

		//绑定添加事件，并定义添加操作  
		if (btn) btn.bind("click", function() {
				//RT  - 添加课程类型
				var parentID = treeNode.id.substring(0, 2);
				layer.msg("parentID=" + parentID);
				showAddCourseTypeWindow();
				//CT  - 添加课程分类

				//CA  - 添加专题   
				/* var zTree = $.fn.zTree.getZTreeObj("treeDemo");
				//将新节点添加到数据库中  
				var name = 'NewNode';
				$.post('./index.php?r=data/addtree&pid=' + treeNode.id + '&name=' + name, function(data) {
					var newID = data; //获取新添加的节点Id  
					zTree.addNodes(treeNode, {
						id : newID,
						pId : treeNode.id,
						name : name
					}); //页面上添加节点  
					var node = zTree.getNodeByParam("id", newID, null); //根据新的id找到新添加的节点  
					zTree.selectNode(node); //让新添加的节点处于选中状态  
				}); */
			});
	}
	;
	function removeHoverDom(treeId, treeNode) {
		$("#addBtn_" + treeNode.tId).unbind().remove();
	}
	//打开节点
	function openMyNote(id) {
		var zTree = $.fn.zTree.getZTreeObj("treeDemo");
		var node = zTree.getNodeByParam("id", id, null); //根据新的id找到新添加的节点  
		zTree.selectNode(node); //让新添加的节点处于选中状态  
	}
	//添加课程类型方法
	function showAddCourseTypeWindow() {
		layer.prompt({
			title : '请输入课程名称',
			formType : 0,
			value : ''
		}, function(value, index) {
			layer.close(index);
			//调用添加课程方法
			$.ajax({
				url : '${pageContext.request.contextPath}/course/addCourseType.do',
				type : 'post',
				cache : false,
				data : {
					"name" : value
				},
				success : function(data) {
					if (data.code == 200) {
						openMyNote(data.data.id);
						layer.msg(data.message);
					} else {
						layer.msg(data.message);
					}
				}
			})
		});
	}
</script>
</html>
