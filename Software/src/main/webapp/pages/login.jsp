<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>登录</title>
<meta name="viewport"
	content="width=device-width,initial-scale=1.0,maximum-scale=1.0,minimum-scale=1.0,user-scalable=no">
<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="js/layui-v2.2.6/layui/css/layui.css">
<style type="text/css">
* {
	margin: 0;
	padding: 0;
}

#web_bg {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	min-width: 50%;
	z-index: -10;
	zoom: 1;
	background-color: #fff;
	background-repeat: no-repeat;
	background-size: cover;
	-webkit-background-size: cover;
	-o-background-size: cover;
	background-position: center 0;
}

.centerDiv {
	margin-top: 500px;
	margin-left: 500px;
}

.mainDiv {
	background-color: #fff;
	margin-left: 38%;
	padding-top: 20px;
	margin-top: 15%;
	width: 400px;
	height: 350px;
	border-radius: 20px;
}

.login-title {
	border-top-left-radius: 20px; /*边框顶部圆角左  */
	border-top-right-radius: 20px;
	margin-top: -10%;
	margin-bottom: 20px;
	height: 40px;
	background-color: #2f7394;
}
</style>
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/layui-v2.2.6/layui/layui.all.js"></script>
<script type="text/javascript" src="js/crypto-js-develop/src/core.js"></script>
<script type="text/javascript" src="js/crypto-js-develop/src/md5.js"></script>
<script type="text/javascript">
	$(function() {

		$(document).keypress(function(e) {
	       var eCode = e.keyCode ? e.keyCode : e.which ? e.which : e.charCode;
	        if (eCode == 13){
	            login();
	        }
		});
	});
	//layer.msg("${pageContext.request.contextPath}");basePath")
	function login() {
		var username = $("#username").val();
		var password = $("#password").val();
		if (username == null || username == "") {
			layer.msg("用户名不可为空");
			return false;
		}
		if (username.length <= 5 || username.length >= 17) {
			layer.msg("用户名长度有误");
			return false;
		}
		if (password == null || password == "") {
			layer.msg("密码不可为空");
			return false;
		}
		if (password.length <= 5 || password.length >= 17) {
			layer.msg("密码长度有误");
			return false;
		}
		password = CryptoJS.MD5(password).toString(CryptoJS.enc.Hex);
		$.ajax({
			url : "user/login.do",
			type : "post",
			data : {
				"username" : username,
				"password" : password
			},
			success : function(data) {
				if (data.code == 200) {
					layer.msg("登录成功");
					setTimeout(function() {
						window.location.href = "${pageContext.request.contextPath}/pages/after/backgroundMain.jsp";
					}, 2000);
				} else {
					layer.msg(data.message);
				}
			}
		});
	}
</script>
</head>

<body>
	<div class="wrapper">
		<!--背景图片-->
		<div id="web_bg" style="background-image: url(image/background.jpg);"></div>
	</div>
	<div class="mainDiv">
		<div class="login-title">
			<h1 style="padding-top: 8px;">
				<span class="active-title" style="margin-left: 165px;"> <font
					size="4px;" style="KaiTi;" color="#FFEFD5">登&nbsp;&nbsp;&nbsp;&nbsp;录</font>
				</span>
			</h1>
		</div>
		<div class="layui-inline" style="margin:10px">
			<label class="layui-form-label">帐号</label>
			<div class="layui-input-inline">
				<input type="text" id="username" class="layui-input"
					placeholder="请输入登录邮箱/手机号" />
			</div>
		</div>
		<div>
			<div class="layui-inline" style="margin:10px">
				<label class="layui-form-label">密码</label>
				<div class="layui-input-inline">
					<input type="text" id="password" class="layui-input"
						placeholder="请输入6-16位密码" />
				</div>
			</div>
		</div>
		<div style="margin-top : 15px;">
			<label for="auto-signin" class="rlf-autoin l" hidefocus="true"
				style="margin-left:70px;"><input type="checkbox"
				checked="checked" class="auto-cbx" id="auto-signin">7天内自动登录</label>
			<a href="#" class="rlf-forget r" target="_blank" hidefocus="true"
				style="margin-left:58px;">忘记密码 </a>
		</div>

		<div style="margin-top: -10px;">
			<button class="layui-btn" id="login" onclick="login()"
				style="margin-left:80px;margin-top:30px;">登录</button>
			<button class="layui-btn" onclick="register()"
				style="margin-left:70px;margin-top:30px;">注册</button>
		</div>
		<div style="margin: 10%;">
			<span style="color:#666">其他方式登录</span> <a style="margin: 20px;"
				href="javascript:void(0)" hidefocus="true"
				data-login-sns="/passport/user/tplogin?tp=weibo"> <i
				class="layui-icon" style="font-size:30px;">&#xe63a;</i>
			</a> <a style="margin: 10px;font-size:30px;" href=" javascript:void(0)"
				hidefocus="true" data-login-sns="/passport/user/tplogin?tp=weixin">
				<i class="layui-icon" style="font-size:30px;">&#xe63a;</i>
			</a> <a style="margin:15px;font-size:30px;" href=" javascript:void(0)"
				hidefocus="true" data-login-sns="/passport/user/tplogin?tp=qq">
				<i class="layui-icon" style="font-size:30px;">&#xe63a;</i>
			</a>
		</div>
	</div>
</body>
</html>
