--user表

CREATE TABLE `t_user` (
`id`  int NOT NULL AUTO_INCREMENT COMMENT 'user表主键' ,
`name`  varchar(20) CHARACTER SET utf8 NOT NULL ,
`username`  varchar(25) NOT NULL ,
`password`  varchar(50) NOT NULL ,
`token`  varchar(255) NULL DEFAULT '' ,
`tel`  varchar(11) NOT NULL COMMENT '手机号码,必填' ,
`role`  int NOT NULL DEFAULT 0 COMMENT '角色权限' ,
`QQ`  varchar(12) NULL ,
`WeChat`  varchar(50) NULL ,
`head_portrait`  varchar(200) NULL COMMENT '头像图片' ,
`create_date`  datetime NOT NULL ,
`modify_date`  datetime NOT NULL ,
`is_delete`  varchar(255) NOT NULL DEFAULT 0 COMMENT '是否删除;0:默认未删除;1:已删除;' ,
PRIMARY KEY (`id`),
UNIQUE INDEX `查找` (`id`, `username`, `tel`, `QQ`, `WeChat`) 
)
DEFAULT CHARACTER SET=utf8;

-- 系统管理员
INSERT INTO `t_user` VALUES (1, '系统管理员', 'system', '0AFF9BA863F578281E0CDCCD2065B167', '', '13022530620', 0, '1103613624', 'werwrewr', NULL, '2018-5-11 10:27:07', '2018-5-11 10:27:10', '0');
INSERT INTO `t_user` VALUES (2, '管理员', 'root', 'root', '', '0454555', 0, '1103613624', 'root1', '', '2018-5-11 10:27:07', '2018-5-11 10:27:10', '0');
INSERT INTO `t_user` VALUES (3, '张三', 'zhangsan', 'zhangsan', '', '0333333', 0, '1103613624', '12123123', '', '2018-5-11 10:27:07', '2018-5-11 10:27:10', '0');
INSERT INTO `t_user` VALUES (4, '李四', 'lisi44444', 'lisi44444', '', '3241234', 0, 'lisi44444', 'lisi44444', '', '2018-5-11 10:27:07', '2018-5-11 10:27:10', '0');
INSERT INTO `t_user` VALUES (5, '王五', 'wangwu', 'wangwu', '', '165165161', 0, 'wangwu222', 'wangwu34234', '', '2018-5-11 10:27:07', '2018-5-11 10:27:10', '1');
INSERT INTO `t_user` VALUES (6, '赵六', 'zhaoliu', 'zhaoliu', '', '66666666', 0, 'zhaoliu666', 'zhaoliu66666', '', '2018-5-11 10:27:07', '2018-5-11 10:27:10', '0');
INSERT INTO `t_user` VALUES (7, '小7', 'xiao7', '123', '', '2342134', 0, 'xiao7', 'xiao7', NULL, '2018-5-23 12:18:05', '2018-5-23 12:18:08', '0');
INSERT INTO `t_user` VALUES (8, '小8', 'xiao7', '123', '', '2342134', 0, 'xiao8', 'xiao8', '', '2018-5-23 12:18:05', '2018-5-23 12:18:08', '0');
INSERT INTO `t_user` VALUES (9, '小9', 'xiao7', '123', '', '2342134', 0, 'xiao9', 'xiao9', '', '2018-5-23 12:18:05', '2018-5-23 12:18:08', '0');
INSERT INTO `t_user` VALUES (10, '小10', 'xiao7', '123', '', '2342134', 0, 'xiao10', 'xiao10', '', '2018-5-23 12:18:05', '2018-5-23 12:18:08', '1');
INSERT INTO `t_user` VALUES (11, '小11', 'xiao11', '123', '', '2342134', 0, 'xiao11232', 'xiao11', '', '2018-5-23 12:18:05', '2018-5-23 12:18:08', '1');

