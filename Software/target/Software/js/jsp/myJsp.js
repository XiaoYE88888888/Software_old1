/**
 * MyJsp.jsp
 */
$(function() {
	var myPlayer = videojs('player2');
	videojs("player2").ready(function() {
		var myPlayer = this;
		myPlayer.play();
	});
	
});

function getPeopleList() {
	$.ajax({
		url : "getPeopleList.do",
		type : "POST",
		dataType : "json",
		data : {
			"id" : 1
		},
		contentType : "application/json",
		success : function(data) {
			var html = "";
			for (var i = 0; i < data.length; i++) {
				html += "<tr><td>" + data[i].name + "</td><td>" + data[i].age + "</td><td>" + data[i].sex + "</td></tr>"
			}
			$("#tbody").html(html)
		}
	});
}
function getPeople() {
	$.ajax({
		url : "getPeople.do",
		type : "POST",
		dataType : "json",
		data : {
			"id" : 1
		},
		contentType : "application/json",
		success : function(data) {
			alert(data.name);
		}
	});
}
function funPhone() {
	var text1 = $("#text1").val();
	var patrn = /^0?(13[0-9]|15[012356789]|18[0236789]|14[57])[0-9]{8}$/;
	if (!patrn.exec(text1)) {
		alert(false);
		return;
	}
	alert(true);

}
function funEmail() {
	var text2 = $("#text2").val();
	var patrn = /^([0-9A-Za-z\-_\.]+)@([0-9A-Za-z]+\.[A-Za-z]{2,3}(\.[A-Za-z]{2})?)$/g;
	if (!patrn.exec(text2)) {
		alert(false);
		return;
	}
	alert(true);

}
function MD5() {
	var MD5 = CryptoJS.MD5($("#MD5In").val()).toString(CryptoJS.enc.Hex);
	$("#MD5Out").val(MD5);
}
function selectStudentList() {
	var html = "";
	$.ajax({
		url : "user/selectUserList.do",
		type : "post",
		success : function(data) {
			if (data.code != 200) {
				layer.msg(data.message);
			} else {
				$.each(data.returnData, function(i, user) {
					var flag = "否";
					if (user.isDelete != 0) {
						flag = "是";
					}
					html += "<tr>" +
						"<td style='text-align: center;'>" + user.id + "</td>" +
						"<td style='text-align: center;'>" + user.name + "</td>" +
						"<td style='text-align: center;'>" + user.username + "</td>" +
						"<td style='text-align: center;'>" + user.token + "</td>" +
						"<td style='text-align: center;'>" + user.tel + "</td>" +
						"<td style='text-align: center;'>" + flag + "</td>" +

						+"</tr>"
				});
				$("#tbody2").html(html);

			}
		}
	});

}
