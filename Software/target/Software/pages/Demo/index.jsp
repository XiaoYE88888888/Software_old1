<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<title>中国家庭教育管理后台</title>
<link rel="stylesheet"
	href="js/layui-v2.2.6/layui/css/layui.css" media="all">
<style type="text/css">
*{
	margin: 0px;
	padding:0px; 
	
	}
</style>  
</head>
<script src="js/jquery-3.1.1.min.js" type="text/javascript" media="all"></script>
<script src="js/layui-v2.2.6/layui/layui.all.js" type="text/javascript" media="all"></script>
<script src="js/layer/layer.js"></script>
<script src="layui/layui.js"></script>
<script type="text/javascript">
$(function(){
	getUserMessage();
	leftFold();
	liToggleClass();
	
})
<%-- 登陆以后获取用户信息 --%>
function getUserMessage(){
	$.ajax({
					url : "${pageContext.request.contextPath}/rest/common/getInfo.do",
					type : 'get',
					cache : false,
					success : function(data) {
						if (data.code == 100) {
							data = data.data;
							$("#userName").html(data.name);
							$("#userCompany").html("("+data.company+")");
							var image = data.image;
							if (image != null && image.length > 0) {
								$('#image').attr('src', image);
							}
	
						} else {
							layer.msg(data.data, {
								time : 1000
							});
						}
					}
				});
	
}
layui.use('element', function() {
			var element = layui.element;
	
		});
var isShow = true;  //定义一个标志位
<%-- 左侧栏 --%>
function leftFold(){
    $('.kit-side-fold').click(function(){
    	shouSuo();
    });
}
<%--控制左侧功能菜单的收缩 --%>
function shouSuo(){
		$("#xy").toggleClass("layui-icon-next");
        $("#xy").toggleClass("layui-icon-prev");
	//选择出所有的span，并判断是不是hidden
	$('.layui-nav-item span').each(function(){
	            if($(this).is(':hidden')){
	                $(this).show();
	            }else{
	            	$(".layui-nav-item").removeClass("layui-nav-itemed");
	                $(this).hide();
	            }
	        });
	 //判断isshow的状态
        if(isShow){
            $('.layui-side.layui-bg-black').width(60); //设置宽度
            $('.kit-side-fold i').css('margin-right', '70%');  //修改图标的位置
            //将footer和body的宽度修改
            $('.layui-body').css('left', 60+'px');
            $('.layui-footer').css('left', 60+'px');
            //将二级导航栏隐藏
            $('dd span').each(function(){
                $(this).hide();
            });
            //修改标志位
            isShow =false;
        }else{
            $('.layui-side.layui-bg-black').width(200);
            $('.kit-side-fold i').css('margin-right', '10%');
            $('.layui-body').css('left', 200+'px');
            $('.layui-footer').css('left', 200+'px');
            $('dd span').each(function(){
                $(this).show();
            });
            isShow =true;
        }
}
<%-- 一级菜单单击后的样式 --%>
function liToggleClass(){
		$("#leftNav").children("li").click(function(){
		$("#leftNav").children("li").removeClass("layui-nav-itemed");
		$(this).addClass("layui-nav-itemed");
		if(!isShow){
			shouSuo();
		}
	});
}
</script>
<body class="layui-layout-body">
	<div class="layui-layout layui-layout-admin">
		<div class="layui-header">
			<div class="layui-logo">中国家庭教育管理后台</div>
			<!-- 头部区域（可配合layui已有的水平导航） -->
			<ul class="layui-nav layui-layout-right">
				<li class="layui-nav-item"><a href="javascript:;"> <img
						id="image" src="img/logo.png" class="layui-nav-img"> <span
						id="userName"></span><span id="userCompany"></span>
				</a>
				<li class="layui-nav-item"><a
					href="${pageContext.request.contextPath}/rest/index/exit.do">退出</a></li>
			</ul>
		</div>

		<div class="layui-side layui-bg-black">
			<div class="layui-side-scroll">
				<div title="菜单缩放" class="kit-side-fold"><i id ="xy" class="layui-icon layui-icon-prev" aria-hidden="true" style="margin-left: 8%;font-size: 20px;"></i></div>
				<!-- 左侧导航区域（可配合layui已有的垂直导航） -->
				<ul class="layui-nav layui-nav-tree" lay-filter="demo" id="leftNav">
					<c:if test="${cookie.type.value==6}">
						<li class="layui-nav-item layui-nav-itemed">
							<a  href="index_v3.jsp" target="iframe0"> <i class="layui-icon">&#xe68e;</i>&nbsp;&nbsp;<span>首页</span>
							</a>
						</li>
					</c:if>
					<c:if test="${cookie.type.value>6}">
						<li class="layui-nav-item layui-nav-itemed">
							<a  href="index_v2.jsp" target="iframe0"> <i class="layui-icon">&#xe68e;</i>&nbsp;&nbsp;<span>首页</span>
							</a>
						</li>
					</c:if>
					
					<c:if test="${0< cookie.type.value  && cookie.type.value<=4 }">
						<li class="layui-nav-item">
							<a href="javascript:;"><i class="layui-icon">&#xe705;</i><span>&nbsp;&nbsp;内容管理</span>
							</a>
								<dl class="layui-nav-child">
									<dd>
										<a  href="slideshow.jsp"  target="iframe0" ><span>轮播图和广告页</span></a>
									</dd>
									<dd>
										<a  href="article.jsp"  target="iframe0" ><span>文章管理</span></a>
									</dd>
									<dd>
										<a  href="course.jsp"  target="iframe0" ><span>课程管理</span></a>
									</dd>
									<dd>
										<a  href="book.jsp"  target="_blank"><span>书目管理</span></a>
									</dd>
									<dd>
										<a  href="note.jsp"  target="iframe0" ><span>笔记管理</span></a>
									</dd>
									<dd>
										<a  href="notice.jsp"  target="iframe0" ><span>平台公告管理</span></a>
									</dd>
									<dd>
										<a  href="professor.jsp"  target="iframe0" ><span>专家管理</span></a>
									</dd>
									<c:if test="${0 < cookie.type.value  && cookie.type.value <= 3}">
										<dd>
											<a  target="iframe0"><i class="layui-icon"></i><span>推送管理</span></a>
										</dd>
									</c:if>
								</dl>
							</li>
					</c:if>
					<c:if test="${cookie.type.value > 6 }">
							<li class="layui-nav-item">
									<a href="javascript:;"><i class="layui-icon">&#xe62a;</i>&nbsp;&nbsp;<span>公告管理</span><span class="layui-nav-more"></span>
								</a>
								<dl class="layui-nav-child">
									<dd>
										<a href="addnotice.jsp" target="iframe0" ><span>发布管理员公告</span></a>
									</dd>
									<dd >
										<a href="noticeshow.jsp" target="iframe0"><span>查看平台公告</span></a>
									</dd>
								</dl>
							</li>
					</c:if>
					<c:if test="${cookie.type.value == 6 }">
						<li class="layui-nav-item">
							<a href="javascript:;"><i class="layui-icon">&#xe62a;</i>&nbsp;&nbsp;<span>公告管理</span></a>
							<dl class="layui-nav-child">
								<dd>
									<a href="noticeshow.jsp" target="iframe0"><span>查看平台公告</span></a>
								</dd>
							</dl>
					</c:if>
					<c:if test="${cookie.type.value > 6 }">
					<li class="layui-nav-item">
						<a href="javascript:;"><i class="layui-icon">&#xe630;</i>&nbsp;&nbsp;<span>项目管理</span>
					</a>
						<dl class="layui-nav-child">
							<dd>
								<a  href="projectStart.jsp" target="iframe0" ><span>发起项目</span></a>
							</dd>
							<dd>
								<a  href="project.jsp" target="iframe0"><span>项目维护</span></a>
							</dd>
							<dd>
								<a  href="oldProject.jsp" target="iframe0"><span>历史项目</span></a>
							</dd>
						</dl>
					</li>
					</c:if>
	
					<c:if test="${cookie.type.value==6}">
						<li class="layui-nav-item">
							<a href="courseManage.jsp" target="iframe0"> 
								<i class="layui-icon">&#xe705;</i>&nbsp;&nbsp;<span>课程管理</span>
							</a>
						</li>
					</c:if>
	
					<c:if
						test="${0< cookie.type.value  && (cookie.type.value<=3 || cookie.type.value==5 || cookie.type.value>=7)}">
						<li class="layui-nav-item"><a ><i class="layui-icon">&#xe770;</i>&nbsp;&nbsp;<span>用户管理</span></a>
							<dl class="layui-nav-child">
								<c:if test="${0< cookie.type.value  && cookie.type.value<=3}">
									<!-- <li><a  href="student-maintenance.jsp" target="iframe0">学员管理</a></li> -->
									<dd><a  href="student-Inner.jsp" target="iframe0"><span>学员管理</span></a></dd>
								</c:if>
	
								<c:if test="${cookie.type.value>=7}">
									<dd><a  href="student.jsp" target="iframe0"><span>学员维护</span></a></dd>
								</c:if>
	
								<c:if test="${cookie.type.value>=7}">
									<dd><a  href="administrator.jsp" target="iframe0"><span>管理员维护</span></a></dd>
								</c:if>
	
								<c:if test="${cookie.type.value>=7}">
									<dd><a  href="checkPeople.jsp" target="iframe0"><span>作业审核员维护</span></a></dd>
								</c:if>
	
								<c:if test="${0< cookie.type.value  && cookie.type.value<=3 }">
									<dd><a  href="admin.jsp" target="iframe0"><span>管理员维护</span></a></dd>
								</c:if>
	
								<c:if
									test="${0< cookie.type.value  && (cookie.type.value<=3 || cookie.type.value==5)}">
									<dd><a  href="study-find.jsp" target="iframe0"><span>用户查询</span></a></dd>
								</c:if>
							</dl>
						</li>
					</c:if>
	
					<c:if test="${0< cookie.type.value  && cookie.type.value<=3}">
						<li class="layui-nav-item"><a href="javascript:;"> <i class="layui-icon">&#xe63c;</i>&nbsp;&nbsp;<span>资质申请管理</span>
						</a>
							<dl class="layui-nav-child">
								<dd><a  href="qualification.jsp" target="iframe0"><span>资质认证审核</span></a>
								</dd>
								<dd><a  href="additional.jsp" target="iframe0"><span>附加分审核</span></a></dd>
								<dd><a  href="paperAudit.jsp" target="iframe0"><span>论文审核</span></a></dd>
	
							</dl></li>
					</c:if>
	
					<c:if test="${0< cookie.type.value  && ( cookie.type.value<=3)}">
						<li class="layui-nav-item"><a href="javascript:;"><i class="layui-icon">&#xe629;</i>&nbsp;&nbsp;<span>统计分析管理</span></a>
							<dl class="layui-nav-child">
								<dd><a 
									href="studentDataUserCondition.jsp" target="iframe0"><span>数据使用情况总览</span></a></dd>
								<dd><a  href="finish.jsp" target="iframe0"><span>课程完成统计</span></a></dd>
							</dl></dd>
					</c:if>
	
					<c:if test="${cookie.type.value>=7}">
						<li class="layui-nav-item"><a href="javascript:;"><i class="layui-icon">&#xe6b2;</i>&nbsp;&nbsp;<span>考核管理</span></a>
							<dl class="layui-nav-child" target="iframe0">
								<dd><a  href="task.jsp" target="iframe0"><span>作业考核管理</span></a></dd>
								<dd><a  href="exam.jsp" target="iframe0"><span>指导者考试管理</span></a></dd>
							</dl></li>
					</c:if>
					<c:if test="${cookie.workType.value>0}">
						<li class="layui-nav-item"><a  href="checkWorkDetail.jsp" target="iframe0"><i
								class="layui-icon">&#xe642;</i>&nbsp;&nbsp;<span>作业审核详情</span></a></li>
					</c:if>
					<c:if test="${cookie.type.value>=7}">
						<li class="layui-nav-item"><a href="javascript:;"><i class="layui-icon">&#xe629;</i>&nbsp;&nbsp;<span>数据分析管理</span></a>
							<dl class="layui-nav-child">
								<dd><a  href="account.jsp" target="iframe0"><span>账号开通情况</span></a></dd>
								<dd><a  href="courseFinish.jsp" target="iframe0"><span>课程完成情况</span></a></dd>
								<dd><a  href="study_time.jsp" target="iframe0"><span>学习时段统计</span></a></dd>
							</dl>
						</li>
					</c:if>
	
					<c:if test="${cookie.type.value>=0}">
						<li class="layui-nav-item"><a href="javascript:;"><i class="layui-icon">&#xe66f;</i>&nbsp;&nbsp;<span>个人中心</span></a>
							<dl class="layui-nav-child">
								<dd><a  href="accountInfo.jsp" target="iframe0"><span>账号信息</span></a></dd>
								<dd><a  href="changepassword.jsp" target="iframe0"><span>修改密码</span></a></dd>
							</dl>
						</li>
					</c:if>
	
					<c:if test="${cookie.type.value>6}">
						<li class="layui-nav-item"><a  href="help.jsp" target="iframe0"> <i class="layui-icon">&#xe606;</i>&nbsp;&nbsp;<span>帮助中心</span>
						</a></li>
					</c:if>
	
					<c:if test="${cookie.type.value>=6}">
						<li class="layui-nav-item"><a  href="callme.jsp" target="iframe0"><i class="layui-icon">&#xe678;</i>&nbsp;&nbsp;<span>联系我们</span>
						</a></li>
					</c:if>
	
					<c:if
						test="${0< cookie.type.value && (cookie.type.value<=2 || cookie.type.value==5)}">
						<li class="layui-nav-item"><a href="javascript:;"><i class="layui-icon">&#xe6b2;</i> &nbsp;&nbsp;<span>反馈管理</span></a>
							<dl class="layui-nav-child">
								<dd><a  href="opinion.jsp" target="iframe0"><span>反馈管理</span></a></dd>
	
							</dl></li>
					</c:if>
	
					<c:if test="${cookie.type.value>=6}">
						<li class="layui-nav-item"><a  href="feedback.jsp" target="iframe0"> <i
								class="layui-icon">&#xe6b2;</i> &nbsp;&nbsp;<span>意见反馈</span>
						</a></li>
					</c:if>
				</ul>
				
			</div>
		</div>
		<div id = "mainArea"class="layui-body layui-tab-content site-demo site-demo-body" style="margin-top:-10px;margin-bottom:-250px;margin-left: -45px;">
			<!-- 内容主体区域 -->
			<div style="height:100%;width:100%;margin-bottom: 200px;">
				<c:if test="${cookie.type.value==6 }">
					<iframe  name="iframe0" width="100%" height="100%"
						src="index_v3.jsp" frameborder="0" seamless scrolling="no" ></iframe>
				</c:if>
				<c:if test="${0 < cookie.type.value && cookie.type.value < 6 }">
					<iframe  name="iframe0" width="100%" height="100%"
						src="index_v1.jsp" frameborder="0" seamless scrolling="no"  style="margin-left: 1.8%;"></iframe>
				</c:if>
				<c:if test="${cookie.type.value > 6 }">
					<iframe  name="iframe0" width="100%" height="100%"
						src="index_v2.jsp" frameborder="0" seamless scrolling="no" style="margin-left: 1.8%;"></iframe>
				</c:if>
				<c:if test="${cookie.workType.value > 0 && cookie.type.value ==0 }">
					<iframe  name="iframe0" width="100%" height="100%"
						src="checkWorkDetail.jsp" frameborder="0" seamless scrolling="no" style="margin-left: 1.8%;"></iframe>
				</c:if>

			</div>
		</div>
			<div class="layui-footer">
			</div>
		</div>

</body>
</html>