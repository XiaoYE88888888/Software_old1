<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
<title>用户信息表</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="js/layui-v2.2.6/layui/css/layui.css" media="all">
</head>
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/layui-v2.2.6/layui/layui.all.js"></script>
<script type="text/html" id="barDemo">
			<a class="layui-btn layui-btn-xs" lay-event="detail">查看</a>
            <a class="layui-btn layui-btn-xs" lay-event="edit">修改</a>
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
</script>
<script type="text/javascript">
  
  
	$(function(){
		layui.use(['laydate', 'laypage', 'layer', 'table', 'carousel', 'upload', 'element'], function(){
			  var laydate = layui.laydate //日期
			  ,laypage = layui.laypage //分页
			  ,layer = layui.layer //弹层
			  ,table = layui.table //表格
			  ,carousel = layui.carousel //轮播
			  ,upload = layui.upload //上传
			  ,element = layui.element; //元素操作
			  
			  //监听Tab切换
			  element.on('tab(demo)', function(data){
			    layer.msg('切换了：'+ this.innerHTML);
			    console.log(data);
			  });
		  
		   //执行一个 table 实例
			  table.render({
			    elem: '#userList'
			    ,height: 'full'
			    ,url: 'user/selectUserList.do' //数据接口
			    ,method: 'POST'
			    ,data : 'returnDate'
			    ,page: true //开启分页
			    ,cols: [[ //表头
			       {checkbox : true}
			      ,{field: 'id', title: 'ID', width:50, sort: true, fixed: 'left'}
			      ,{field: 'name', title: '用户名', width:200}
			      ,{field: 'username', title: '账号', width:200, sort: true}
			      ,{field: 'tel', title: '电话', width:200} 
			      ,{field: 'QQ', title: 'QQ', width: 200}
			      ,{field: 'WeChat', title: '微信', width: 200, sort: true}
			      ,{field: 'role', title: 'role', width: 80, sort: true}
			      ,{field: 'createDate', title: '创建时间', width: 200}
			      ,{field: 'isDelete', title: '是否删除', width: 200, sort: true}
			      ,{fixed: 'right', title:'操作' , width: 200, align:'center', toolbar: '#barDemo'}
			    ]],
			    where : {
			    //传值 startDate : startDate,
				},
				    response : {
					statusCode : 200,
					msgName : 'message',
					countName:'totalCount',
					
				}
			  });
			   //监听工具条
			  table.on('tool(userList)', function(obj){ //注：tool是工具条事件名，test是table原始容器的属性 lay-filter="对应的值"
			    var data = obj.data //获得当前行数据
			    ,layEvent = obj.event; //获得 lay-event 对应的值
			    if(layEvent === 'detail'){
			      layer.msg('查看操作');
			    } else if(layEvent === 'del'){
			      layer.confirm('真的删除行么', function(index){
			        obj.del(); //删除对应行（tr）的DOM结构
			        layer.close(index);
			        //向服务端发送删除指令
			      });
			    } else if(layEvent === 'edit'){
			      layer.msg('编辑操作');
			    }
			  });
		});
	});
</script>
<body>
	<fieldset class="layui-elem-field layui-field-title"
		style="margin-top: 20px;">
		<legend>默认表格</legend>
	</fieldset>
	<div>
		<table class="layui-hide" id="userList" lay-filter="userList"></table>
	</div>
	<!-- <div class ="layui-form">
		<table class="layui-table">
			<colgroup>
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
				<col width="10%">
			</colgroup>
			<thead>
				<tr>
					<th>ID</th>
					<th>姓名</th>
					<th>username</th>
					<th>tel</th>
					<th>QQ</th>
					<th>WeChat</th>
					<th>创建时间</th>
					<th>是否删除</th>
					<th>操作</th>
				</tr>
			
			</thead>
			<tbody id="tbody">
			</tbody>
		</div>
	</div> -->
</body>
</html>
