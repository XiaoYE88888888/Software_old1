<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
	String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort()
			+ path + "/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<base href="<%=basePath%>">

<title>后台管理系统</title>

<meta http-equiv="pragma" content="no-cache">
<meta http-equiv="cache-control" content="no-cache">
<meta http-equiv="expires" content="0">
<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
<meta http-equiv="description" content="This is my page">
<meta charset="utf-8">
<meta name="renderer" content="webkit">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<link rel="stylesheet" type="text/css"
	href="js/layui-v2.2.6/layui/css/layui.css" media="all">
<script type="text/javascript" src="js/jquery-3.1.1.min.js"></script>
<script type="text/javascript" src="js/layui-v2.2.6/layui/layui.all.js"></script>
</head>

<body>
	<div id="LAY_app">
		<div class="layui-layout layui-layout-admin">
			<div class="layui-header">
				<!-- 头部区域 -->
				<ul class="layui-nav layui-layout-left">
					<li class="layui-nav-item layadmin-flexible" lay-unselect="">
						<a href="javascript:;" layadmin-event="flexible" title="侧边伸缩">
							<i class="layui-icon layui-icon-shrink-right"
							id="LAY_app_flexible"></i>
					</a>
					</li>
					<li class="layui-nav-item layui-hide-xs" lay-unselect=""><a
						href="http://www.layui.com/admin/" target="_blank" title="前台">
							<i class="layui-icon layui-icon-website"></i>
					</a></li>
					<li class="layui-nav-item" lay-unselect=""><a
						href="javascript:;" layadmin-event="refresh" title="刷新"> <i
							class="layui-icon layui-icon-refresh-3"></i>
					</a></li>
					<span class="layui-nav-bar"
						style="width: 0px; left: 0px; opacity: 0;"></span>
				</ul>
				<ul class="layui-nav layui-layout-right"
					lay-filter="layadmin-layout-right">

					<li class="layui-nav-item" lay-unselect=""><a
						lay-href="http://www.layui.com/admin/std/dist/views/app/message/index.html"
						layadmin-event="message" lay-text="消息中心"> <i
							class="layui-icon layui-icon-notice"></i> <!-- 如果有新消息，则显示小圆点 -->
							<span class="layui-badge-dot"></span>
					</a></li>
					<li class="layui-nav-item layui-hide-xs" lay-unselect=""><a
						href="javascript:;" layadmin-event="theme"> <i
							class="layui-icon layui-icon-theme"></i>
					</a></li>
					<li class="layui-nav-item layui-hide-xs" lay-unselect=""><a
						href="javascript:;" layadmin-event="note"> <i
							class="layui-icon layui-icon-note"></i>
					</a></li>
					<li class="layui-nav-item" lay-unselect=""><a
						href="javascript:;"> <cite>贤心</cite> <span
							class="layui-nav-more"></span></a>
						<dl class="layui-nav-child">
							<dd>
								<a
									lay-href="http://www.layui.com/admin/std/dist/views/set/user/info.html">基本资料</a>
							</dd>
							<dd>
								<a
									lay-href="http://www.layui.com/admin/std/dist/views/set/user/password.html">修改密码</a>
							</dd>
							<hr>
							<dd layadmin-event="logout" style="text-align: center;">
								<a>退出</a>
							</dd>
						</dl></li>

					<li class="layui-nav-item layui-hide-xs" lay-unselect=""><a
						href="javascript:;" layadmin-event="about"><i
							class="layui-icon layui-icon-more-vertical"></i></a></li>
					<li class="layui-nav-item layui-show-xs-inline-block layui-hide-sm"
						lay-unselect=""><a href="javascript:;" layadmin-event="more"><i
							class="layui-icon layui-icon-more-vertical"></i></a></li>
					<span class="layui-nav-bar"></span>
				</ul>
			</div>

			<!-- 侧边菜单 -->
			<div class="layui-side layui-side-menu">
				<div class="layui-side-scroll">
					<div class="layui-logo"
						lay-href="http://www.layui.com/admin/std/dist/views/home/console.html">
						<span>username</span>
					</div>

					<ul class="layui-nav layui-nav-tree" lay-shrink="all"
						id="LAY-system-side-menu" lay-filter="layadmin-system-side-menu"
						style="height:100%">
						<li data-name="home" class="layui-nav-item"><a
							href="javascript:;" lay-tips="主页" lay-direction="2"> <i
								class="layui-icon layui-icon-home"></i> <cite>主页</cite> <span
								class="layui-nav-more"></span></a>
							<dl class="layui-nav-child">
								<dd data-name="console" class="layui-this">
									<a
										lay-href="http://www.layui.com/admin/std/dist/views/home/console.html">控制台</a>
								</dd>
							</dl></li>
					</ul>
				</div>
			</div>

			
			<!-- 主体内容 -->
			<div class="layui-body" id="LAY_app_body">
				<div class="layadmin-tabsbody-item layui-show">
					<iframe
						src="http://www.layui.com/admin/std/dist/views/home/console.html"
						frameborder="0" class="layadmin-iframe"
						style="width:100%;height:100%"></iframe>
				</div>
	</div>

	<!-- 辅助元素，一般用于移动设备下遮罩 -->
	<div class="layadmin-body-shade" layadmin-event="shade"></div>
	</div>
	</div>
</body>
</html>
